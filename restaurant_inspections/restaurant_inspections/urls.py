from django.conf.urls import include, url
from django.contrib import admin
from inspections import views
urlpatterns = [
    url(r'^inspections/', include('inspections.urls')),
    url(r'^$', views.all_restaurants),
    url(r'^admin/', include(admin.site.urls)),
]






