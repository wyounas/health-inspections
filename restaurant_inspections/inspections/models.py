from django.db import models

# Create your models here.


class Restaurant(models.Model):
    name = models.CharField(max_length=500)
    last_updated = models.DateTimeField(auto_now_add=True)

class Inspection(models.Model):
    name = models.CharField(max_length=500)
    date = models.DateTimeField('Inspection Date', null=True, blank=True)
    violations = models.IntegerField(default=0)
    restaurant = models.ForeignKey(Restaurant)
    last_updated = models.DateTimeField(auto_now_add=True)

