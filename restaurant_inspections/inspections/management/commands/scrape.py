"""EHInspectionScrapper is a wrapper class that fetches data from health.state.tn.us given a 
search term. URL and other stuff is hardcoded, could be picked from config file.
First, we get list of all inspections for the Restaurant and we finally track down
all inspections for every violation.
"""
import time
import re
import requests
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from inspections.models import Restaurant, Inspection

class EHInspectionScrapper(object):
    def __init__(self, search_term):
        assert search_term is not None
        self.records = None
        self.total_rows = []
        #for now hardcoding URLs, could have picked it from a config file
        self.url = "http://health.state.tn.us/EHInspections/EHInspections/GetSearchResults?ProgramID=0605&SearchTerms=%s&_search=false&nd=1402988524433&rows=25&sidx=&sord=asc"
        self.url = self.url % (search_term)
        self.inspection_url = "http://health.state.tn.us/EHInspections/EHInspections/GetInspections?ProgramID=0605&LicenseNbr=%s&_search=false&nd=1402992062965&sidx=&sord=asc"
        self.data = {} 

    
    def get_data(self):
        max_retries = 2
        #wish I had the time to replace this retry code with retry decorator
        for idx in range(0, max_retries):
            try:
                response = requests.get(self.url)
            except Exception, e:
                if idx < max_retries:
                    continue # we can still retry
                raise e
            try:
                json = response.json()
            except ValueError, e:
                if idx < max_retries:
                    continue #we can still retry
                raise e
            else:
                self.records = json['records']
                self.total_rows = requests.get(self.url).json()['rows']
                return self.get_inspections_data() #return data, no need to retry
    def _get_inspection(self, inspection_rows):
        inspections = []
        for row in inspection_rows:#iterate through inspection data
            inspection = {}
            cell_data = row['cell']
            #using a regex to get violation's occurences from a list that
            #looks like ["0605181970","304307","Complete (Food)","08/09/2011","0 VIOLATIONS","0 VIOLATIONS","92"]
            violations = [int(re.match(r"^(\d+) VIOLATION", item).groups(0)[0]) \
                    for item in cell_data if re.match(r"^(\d+) VIOLATION", item)]
            violations = sum(violations)
            #violations is number of violation in this particular inspection
            inspection['violation'] = (violations)
            inspection['name'] = cell_data[2]#name of inspection
            inspection['datetime'] =cell_data[3]#datetime of inspection
            inspections.append(inspection)
        return inspections
 
    def get_inspections_data(self):
        """Data[] will be a list of dicts and each dict
        will contain name, license_nbr and inspections as key. 
        The key 'inspections' will be a list containing data
        related to each inspection
        """
        data = []
        counter = 0
        for row in self.total_rows:
            row_data = {}
            license_nbr = row['i']
            row_data['name'] = row['cell'][1] + row['cell'][2]
            #license_nbr is usually given as 06050605033211, we strip
            #first four characters so we can use it in inspection_url
            row_data['license_nbr'] = row['i'][4:]
            inspection_url = self.inspection_url % (row_data['license_nbr'])
            rows = requests.get(inspection_url).json()['rows']
            time.sleep(0.2)
            inspections = self._get_inspection(rows)
            row_data['inspections'] = inspections
            data.append(row_data)
        return data



class Command(BaseCommand):

    def add_arguments(self, parser):
        pass 
    def handle_data(self, data):
        """We iterate through a list of dictionaries where each
        dictionary contain 'inspections'. We iterate through
        each inspection and record inspection and violations data
        """
        for item in data:
            restaurant_name = item['name']
            inspections = item['inspections']
            restaurant, created = Restaurant.objects.get_or_create(name=restaurant_name)
            for inspect in inspections:
                inspection_date = None if not inspect['datetime'] \
                        else datetime.strptime(inspect['datetime'], "%m/%d/%Y")
                inspection = Inspection.objects.get_or_create(name=inspect['name'], 
                        date=inspection_date, 
                        violations=inspect['violation'],
                        restaurant=restaurant)

    def handle(self, *args, **options):
        search_term = args[0]
        assert search_term is not None #cannot do search without a search term
        scrapper = EHInspectionScrapper(search_term)
        print "Scrapping data..."
        data = scrapper.get_data()
        print "Saving..."
        self.handle_data(data)

