from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from inspections.models import Restaurant, Inspection

def all_restaurants(request):
    restaurants = Restaurant.objects.all()
    template = loader.get_template('restaurants.html')
    context = RequestContext(request, {
        'restaurants': restaurants,
    })
    return HttpResponse(template.render(context))

def get_inspections(request, restaurant_id):
    restaurant = Restaurant.objects.get(pk=restaurant_id)
    inspections= Inspection.objects.filter(restaurant=restaurant)
    template = loader.get_template('inspections.html')
    context = RequestContext(request, {
        'inspections': inspections,
        })
    return HttpResponse(template.render(context))
