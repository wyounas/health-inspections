from django.conf.urls import patterns, include, url

from django.contrib import admin
from inspections import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'restaurant_inspections.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    
    url(r'^(?P<restaurant_id>[0-9]+)/$', views.get_inspections, ),
)
